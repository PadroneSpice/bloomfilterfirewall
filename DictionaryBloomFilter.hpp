#ifndef DICTIONARY_BLOOM_FILTER_HPP
#define DICTIONARY_BLOOM_FILTER_HPP
#include <string>
#include <vector>
#include <array>

/**
 * Made by Sean Castillo for programming assignment
 */
class DictionaryBloomFilter {
public:
    
    /* Create a new bloom filter with the given table size in bytes */
    DictionaryBloomFilter(size_t tableSize);

    /* Insert a word into the bloom filter */
    bool insert(std::string word);

    /* Determine whether a word is in the bloom filter */
    bool find(std::string word) const;
    
    /* For benchmarking the hash functions*/
    void hashBenchmark();

    /* Destructor for the bloom filter */
    ~DictionaryBloomFilter();
    
    /* Arrays to keep track of how much each hash value is generated */
    // harder to do with hashes A and C because the table size is dynamic,
    // so using a sample version
    unsigned int hashBRecord[8] = {0};
    unsigned int hashDRecord[8] = {0};
    unsigned int tableSampleRecordA[64] = {0};
    unsigned int tableSampleRecordC[120] = {0};
    

private:
    unsigned int size;
    unsigned char* table;

    /** The Sean System: 4 Hash Functions
     *  Function A: Byte 1
     *  Function B: Bit  1
     *  Function C: Byte 2
     *  Function D: Bit  2
     */
    unsigned int hashFunctionA(std::string word) const;
    unsigned int hashFunctionB(std::string word) const;
    unsigned int hashFunctionC(std::string word) const;
    unsigned int hashFunctionD(std::string word) const;
    /* Calculates ten's complements of every digit in a number  */
    unsigned int tensComplements(unsigned int num) const;
    /* Checks whether a given bit is set to 1 */
    bool bitIsSet(char c, unsigned int n) const;

};

#endif // DICTIONARY_BLOOM_FILTER
