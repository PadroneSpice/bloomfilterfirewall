#include "DictionaryBloomFilter.hpp"
#include <string>
#include <math.h>
#include <iostream>
#include <bitset>
#include <cstring>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

/**
 * Bloom Filter Firewall by Sean Castillo for programming assignment
 * Hash Function Sources: http://www.cse.yorku.ca/~oz/hash.html
 * http://www.partow.net/programming/hashfunctions/#Introduction
 * http://www.eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx
 */

using namespace std;

/* Create a new bloom filter with the given table size in bytes */
DictionaryBloomFilter::DictionaryBloomFilter(size_t tableSize)
{
  table = new unsigned char[tableSize]();
  size = tableSize;
}

/* Insert a word into the bloom filter */
bool DictionaryBloomFilter::insert(std::string word)
{
  /** First, get the hashes */
  unsigned int h1 = hashFunctionA(word);
  unsigned int h2 = hashFunctionB(word);
  unsigned int h3 = hashFunctionC(word);
  unsigned int h4 = hashFunctionD(word);
  
  //record hash value generated for benchmarking
  hashBRecord[h2] = hashBRecord[h2] + 1;
  hashDRecord[h4] = hashDRecord[h4] + 1;
  
  //Uncomment these when benchmarking
  //if (h1 < 64)
  //{
  //  tableSampleRecordA[h1] = tableSampleRecordA[h1] + 1;
  //}
  //if (h3 < 119)
  //{
  //  tableSampleRecordC[h3] = tableSampleRecordA[h3] + 1;
  //}
    
  //cout << "hash pair 1 (byte, bit): " << h1 << ", " << h2 << endl;
  //cout << "hash pair 2 (byte, bit): " << h3 << ", " << h4 << endl;
  
  //bitHash is the nth bit (from right to left)
  unsigned int bitHashAB = 0;
  unsigned int bitHashCD = 0;
  bitHashAB |= 1U << h2;
  bitHashCD |= 1U << h4;
  
  // Insert in the table based on the hash functions (A,B) and (C,D)
  table[h1] = table[h1] | bitHashAB;
  table[h3] = table[h3] | bitHashCD;
  
  //bitset<8> k(table[h1]);
  //cout << "first byte is now: " << k << endl;

  //bitset<8> j(table[h3]);
  //cout << "second byte is now: " << j << endl;
  
  return true;
}

 /* Determine whether a word is in the bloom filter */
bool DictionaryBloomFilter::find(std::string word) const
{
  //cout << "Word received is: " << word << endl;
  
  //first, get the hash values for that word
  unsigned int h1 = hashFunctionA(word);
  unsigned int h2 = hashFunctionB(word);
  unsigned int h3 = hashFunctionC(word);
  unsigned int h4 = hashFunctionD(word);
  
  bool ABset = false;
  bool CDset = false;
  if (bitIsSet(table[h1], h2)) { ABset = true; }
  if (bitIsSet(table[h3], h4)) { CDset = true; }
  
  //cout << "Expected hashes: " << endl;
  //cout << "(A,B): " << h1 << " " << h2 << endl;
  //cout << "(C,D): " << h3 << " " << h4 << endl;
  
  if (ABset && CDset)
  {
    return true;
  }
  return false;
}

/** Hash FuncSean A: One-at-a-time Hash */
unsigned int DictionaryBloomFilter::hashFunctionA(std::string word) const
{
  const char *s = word.c_str();
  unsigned int hash = 0;
  unsigned int i;
  
  for (i=0; i<word.length(); i++)
  {
    hash += s[i];
    hash += ( hash << 10);
    hash ^= (hash >> 6);
  }
  hash += ( hash << 3);
  hash ^= ( hash >> 11);
  hash += (hash << 15);
  
  hash = hash % size;
  return hash;
}

/** Hash FuncSean B: ELF Hash Function with some modifications*/
unsigned int DictionaryBloomFilter::hashFunctionB(std::string word) const
{
  const char *s     = word.c_str();
  unsigned int hash = 0;
  unsigned int x    = 0;
  unsigned int i    = 0;

  for (i = 0; i < word.length(); ++s, ++i)
  {
    hash = (hash << 4) + (*s);
    if ( (x = hash & 0xF0000000L) != 0)
    {
      hash += (x >> 24);
    }
    hash &= ~x;
  }
 
  //this seems to help a lot
  hash = hash % 13;
  
  if (hash > 7)
  { 
    hash = hash % 8;
  }
  
  //put it through tensComplements and function D
  hash = tensComplements(hash);
  hash = hashFunctionD(word);

  return hash;
}

/* Hash FuncSean C: FNV Hash */
unsigned int DictionaryBloomFilter::hashFunctionC(std::string word) const
{
  const char *s = word.c_str();
  unsigned hash = 2166136261;
  unsigned int i;
  
  for (i=0; i<word.length(); i++)
  {
    hash = (hash * 16777619) ^ s[i];
  }
  
  hash = hash % size;
  return hash; 
}

/* hash FuncSean D: DEK Hash Function */
unsigned int DictionaryBloomFilter::hashFunctionD(std::string word) const
{  
  unsigned int hash = size; 
  unsigned int i = 0;
  const char *s = word.c_str();
  for (i=0; i<word.length(); ++s, ++i)
  {
    hash = ((hash << 5) ^ (hash >> 27)) ^ (*s);
  }

  hash = hash % 8;
  return hash;
}

/** FuncSean by Sean to return an int in which each digit is
 *  the ten's complement of the corresponding digit in the input number.
 *  Currently, the return int is also reversed,
 *  but I may keep it that way since it's sufficiently effective.
 */
unsigned int DictionaryBloomFilter::tensComplements(unsigned int num) const
{
  unsigned int fullComplement = 0;
  std::ostringstream os;

  os << num;
  std::string digitString = os.str();
  stringstream ss;
  std::string::iterator it = digitString.begin();
  unsigned int j;
  
  while (it != digitString.end())
  {
    j = *it - '0';
    j = 10 - j;    //get the ten's complement
    ss << j;       //add j to the stringstream
    ++it;
  }
  
  ss >> fullComplement;
  return fullComplement;
}

/* Method needed to check whether bit n is set; source: Stack Overflow */
bool DictionaryBloomFilter::bitIsSet(char c, unsigned int n) const
{
  //bitset<8> b(c);
  //cout << "bitIsSet(): the byte received is: " << b << endl;
  //cout << "bitIsSet(): the bit to check is: " << n << endl;
  return (1 & (c >> n));
}

/* For benchmarking the hash functions */
void DictionaryBloomFilter::hashBenchmark()
{
  
  cout << "Hash Function Benchmarking" << endl;
  
  cout << "Hash Function B Record: " << endl;
  
  for (int i=0; i<8; i++)
  {
    cout << "index " << i << ": " << hashBRecord[i] << endl;
  }
  
  cout << "Hash Function D Record: " << endl;
  
  for (int i=0; i<8; i++)
  {
    cout << "index " << i << ": " << hashDRecord[i] << endl;
  }
  
  cout << "table Sample A Record: " << endl;
  for (int i=0; i<64; i++)
  {
    cout << "index " << i << ": " << tableSampleRecordA[i] << endl;
  }
  
  cout << "table Sample C Record: " << endl;
  for (int i=0; i<119; i++)
  {
    cout << "index " << i << ": " << tableSampleRecordC[i] << endl;
  }
}

/* Destructor for the bloom filter */
DictionaryBloomFilter::~DictionaryBloomFilter()
{
  delete [] table;
}