Bloom Filer Program By Sean Castillo

Filters out URLs/words deemed malicious from a file and stores the result in another file.

To run, use three arguments:

1. A list of terms to be considered malicious or menacing (one per line)

2. A list of terms to filter through (one per line)

3. The file into which the non-malicious words are to be written