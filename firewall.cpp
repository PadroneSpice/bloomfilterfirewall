#include <iostream>
#include <fstream>
#include "DictionaryBloomFilter.hpp"
#include <vector>
#include <math.h>

using namespace std;

/** Made by Sean Castillo for programming assignment */


int main(int argc, char** argv)
{
  if (argc != 4)
  {
    cout << "This program requires exactly 3 arguments (file names)!" << endl;
    cout << "1 - list of malicious urls/bad words to filter out" << endl;
    cout << "2 - list of mixed (good/bad) words to check" << endl;
    cout << "3 - file to write only the good urls/words" << endl;
    return -1;
  }
  
  //open files
  ifstream in1, in2;
  in1.open(argv[1], ios::binary);
  in2.open(argv[2], ios::binary);
  
  
  //Check for empty files
  in1.seekg(0, ios_base::end); 
  unsigned int len = in1.tellg();
  if(len==0) 
  {
    cout << "The file is empty. \n";
    return -1;
  }
  //Resets the stream to beginning of file
  in1.seekg(0, ios_base::beg);
  
  in2.seekg(0, ios_base::end);
  len = in2.tellg();
  if(len==0) 
  {
    cout << "The file is empty. \n";
    return -1;
  }
  //Resets the stream to beginning of file
  in2.seekg(0, ios_base::beg);
  
  
  //checks to see if file was opened
  if(!in1.is_open())
  {
    cout << "Invalid input file. No file was opened. Please try again.\n";
    return -1;
  }
  if(!in2.is_open())
  {
    cout << "Invalid input file. No file was opened. Please try again.\n";
    return -1;
  }
  
  //use input file 1 to build the table
  //first, get the size from in1
  string line;
  unsigned int size = 0;
  while (!in1.eof())
  {
    getline(in1, line);
    if (!line.empty())
    {
      size++; 
    }
  }
  
  //increase byte size
  //unsigned int badCount = size; //copy original size first for stats
  size = ceil(size * 1.5);
  
  //then close in1 and build the table
  in1.close();
  DictionaryBloomFilter dbf(size);
  
  //reopen in1 to insert the words into the bloom filter
  ifstream in1again;
  in1again.open(argv[1], ios::binary);
  
  while (!in1again.eof())
  {
    getline(in1again, line);
    if (!line.empty())
    {
      dbf.insert(line);
    }
  }
  in1again.close();
   
  //put in2's lines in a vector; these are the lines to go through the filter
  std::vector<string> mixedWords;
  while (!in2.eof())
  {
    getline(in2, line);
    if (!line.empty()) { mixedWords.push_back(line); }
  }
  in2.close();
  
  //stats
  unsigned int foundCount    = 0;
  unsigned int notFoundCount = 0;
  unsigned int totalCount    = 0;
  
  //open the output file
  ofstream of;
  of.open(argv[3]);
  
  if (!of)
  {
    cout << "Error creating output file..." << endl;
    return -1;
  }
  
  //write the lines that pass the filter to the output file
  //and also update the stats
  vector<string>::iterator it = mixedWords.begin();
  while (it != mixedWords.end())
  {
    if (dbf.find(*it)) { foundCount++; }
    else
    {
      notFoundCount++;
      of << *it << "\n";
    }
    totalCount++;
    ++it;
  }
  of.close();
  
  //stats and benchmarking call
  //cout << "badCount: " << badCount << endl;
  //cout << "foundCount: " << foundCount << endl;
  //cout << "notFoundCount: " << notFoundCount << endl;
  //cout << "totalCount: " << totalCount << endl;
  //cout << "table size: " << size << endl;
  //dbf.hashBenchmark();
  
  return 0;
}
